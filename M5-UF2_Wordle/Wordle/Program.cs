﻿using System;

namespace Wordle
{
    public class Wordle
    {
        static void Main(string[] args)
        {
            int maxIntentos = 6;
            int wordSize = 5;
            bool endGame = false;
            string[] palabras = { "comer", "joven", "mango", "lagos", "atada", "agita", "autos", "animo", "asido", "boses", "bruta", "busca", "cuzma", "pizca", "zombi", "cadiz", "velas", "pieza", "rosal", "huevo" };
            Random rnd = new Random();
            int random = rnd.Next(0, palabras.Length);
            bool closeGame = false;
            string PalabraCorrecta = LoadGame(palabras, random);
            do
            {
                StartGame(PalabraCorrecta, wordSize, maxIntentos, endGame);

                Console.WriteLine("¿Quieres volver a jugar? Si/No");
                string continueGame = Console.ReadLine();

                closeGame = PlayerWantAnotherPlay(continueGame);

            } while (closeGame);

        }


        static void StartGame(string palabraCorrecta, int wordSize, int maxIntentos, bool endGame)
        {
            Console.WriteLine("Bienvenido a mi wordle!!");

            string input;

            int intentos = maxIntentos;

            while (endGame == false)
            {
                if (intentos > 0)
                {
                    input = AskWordToPlayer(intentos, wordSize);

                    if (CheckSizeWord(input, wordSize) == true)

                    {
                        intentos = ConsumirIntento(intentos);

                        if (WinCondition(input, palabraCorrecta))
                        {
                            endGame = true;
                            WinGame(ConsoleColor.Green, palabraCorrecta, GetIntentosTotales(maxIntentos, intentos));
                        }
                        else
                        {
                            for (int i = 0; i < palabraCorrecta.Length; i++)
                            {
                                if (CharInCorrectPosition(palabraCorrecta[i], input[i])) //hay algun caracter en su posicion correcta.
                                {
                                    PrintScreen(ConsoleColor.Green, input[i]);
                                }
                                else
                                {
                                    if (CharIsInWord(palabraCorrecta, input[i])) //hay algun caracter en la palabra correcto pero su posicion es erronea.
                                    {
                                        PrintScreen(ConsoleColor.Yellow, input[i]);
                                    }
                                    else
                                    {
                                        // no hay ningun caracter correcto en la palabra.
                                        PrintScreen(ConsoleColor.Gray, input[i]);
                                    }
                                }
                                if (i == palabraCorrecta.Length - 1)
                                {
                                    Console.WriteLine("");
                                }
                            }
                            Console.ResetColor();
                        }
                    }

                    if (input.Length != wordSize)
                    {
                        Console.WriteLine("Escribe una palabra de tamaño " + wordSize);
                    }
                }
                else
                {
                    endGame = true; //acabar el juego si ha consumido el total de intentos
                }

            }
        }


        /// <summary>
        /// Metodo para saber si el jugador quiere jugar otra partida
        /// </summary>
        /// <param name="continueGame"></param>
        /// <returns></returns>
        public static bool PlayerWantAnotherPlay(string continueGame)
        {
            bool result = false;
            if (continueGame == "Si") result = true;
            return result;

        }

        /// <summary>
        /// Método para comprobar si el caracter existe en la palabra aunque no en la posición correcta
        /// </summary>
        /// <param name="palabraCorrecta"></param>
        /// <param name="caracter"></param>
        /// <returns></returns>
        public static bool CharIsInWord(string palabraCorrecta, char caracter)
        {
            bool result = false;
            if (palabraCorrecta.Contains(caracter)) return true;
            return result;

        }

        /// <summary>
        /// Método para comprobar que el caracter está en la posicion correcta
        /// </summary>
        /// <param name="palabraCorrecta"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool CharInCorrectPosition(char palabraCorrecta, char input)
        {
            bool result = false;
            if (palabraCorrecta == input) return true;
            return result;
        }

        /// <summary>
        /// Método para la condición de ganar partida
        /// </summary>
        /// <param name="input"></param>
        /// <param name="palabraCorrecta"></param>
        /// <returns></returns>
        public static bool WinCondition(string input, string palabraCorrecta)
        {
            bool result = false;
            if (input == palabraCorrecta) result = true;
            return result;
        }

        /// <summary>
        /// Método que pide y devuelve la palabra que escriba el jugador
        /// </summary>
        /// <param name="intentos"></param>
        /// <param name="wordSize"></param>
        /// <returns></returns>
        static string AskWordToPlayer(int intentos, int wordSize)
        {
            Console.WriteLine("Escribe una palabra que tenga " + wordSize + " caracteres");
            Console.WriteLine("Tienes " + intentos + " intentos para adivinar la palabra!!");
            return Console.ReadLine();
        }

        /// <summary>
        /// Metodo auxiliar para imprimir colores en consola
        /// </summary>
        /// <param name="color"></param>
        /// <param name="character"></param>
        static void PrintScreen(ConsoleColor color, char character)
        {
            Console.BackgroundColor = color;
            Console.Write(character);
        }

        /// <summary>
        /// Método auxiliar para imprimir información sobre la victoria por consola
        /// </summary>
        /// <param name="color"></param>
        /// <param name="palabraCorrecta"></param>
        /// <param name="totalIntentosUtilizados"></param>
        static void WinGame(ConsoleColor color, string palabraCorrecta, int totalIntentosUtilizados)
        {
            Console.BackgroundColor = color;
            Console.WriteLine(palabraCorrecta);
            Console.ResetColor();
            Console.WriteLine("felicidades!! Haz descubierto la palabra " + palabraCorrecta + ".");
            Console.WriteLine("Sólo has necesitado " + totalIntentosUtilizados + " intento para acertar.");
        }


        /// <summary>
        /// Método que comprueba si la palabra mide el numero de caracteres requerido por wordSize
        /// </summary>
        /// <param name="input"></param>
        /// <param name="wordSize"></param>
        /// <returns></returns>
        public static bool CheckSizeWord(string input, int wordSize)
        {
            bool result = false;
            if (input.Length == wordSize) result = true;

            return result;
        }

        /// <summary>
        /// Método que resta un intento utilizado
        /// </summary>
        /// <param name="intentos"></param>
        /// <returns></returns>
        public static int ConsumirIntento(int intentos)
        {
            int result = intentos - 1;
            return result;
        }

        /// <summary>
        /// Método que carga la palabra secreta en la partida
        /// </summary>
        /// <param name="palabras"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static string LoadGame(string[] palabras, int pos)
        {
            // return "rosal"; //Lo he hecho con una sola palabra correcta.
            return palabras[pos];
        }

        /// <summary>
        /// Método para obtener el número de intentos totales que quedan al jugador en la partida
        /// </summary>
        /// <param name="maxIntentos"></param>
        /// <param name="intentos"></param>
        /// <returns></returns>
        public static int GetIntentosTotales(int maxIntentos, int intentos)
        {
            return maxIntentos - intentos;
        }
    }
}