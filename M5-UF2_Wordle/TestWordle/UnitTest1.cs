using NUnit.Framework;
using Wordle;

namespace TestWordle
{
    public class Tests
    {
        //PlayerWantAnotherPlay Tests
        [Test]
        public void PlayerWantAnotherPlayPassingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.PlayerWantAnotherPlay("Si"));
            Assert.IsTrue(Wordle.Wordle.PlayerWantAnotherPlay("Si"));
            Assert.IsFalse(Wordle.Wordle.PlayerWantAnotherPlay("No"));
            Assert.AreNotEqual(true, Wordle.Wordle.PlayerWantAnotherPlay("hola"));
        }

        [Test]
        public void PlayerWantAnotherPlayFailingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.PlayerWantAnotherPlay("buenas tardes"));
            Assert.IsTrue(Wordle.Wordle.PlayerWantAnotherPlay("No"));
            Assert.IsFalse(Wordle.Wordle.PlayerWantAnotherPlay("Si"));
            Assert.AreNotEqual(false, Wordle.Wordle.PlayerWantAnotherPlay("hola"));
        }


        //CharIsInWord Tests
        [Test]
        public void CharIsInWordPassingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.CharIsInWord("cadiz", 'd'));
            Assert.IsTrue(Wordle.Wordle.CharIsInWord("mango", 'g'));
            Assert.IsFalse(Wordle.Wordle.CharIsInWord("autos", 'b'));
            Assert.AreNotEqual(true, Wordle.Wordle.CharIsInWord("joven", '2'));
        }

        [Test]
        public void CharIsInWordFailingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.CharIsInWord("agita", 'u'));
            Assert.IsTrue(Wordle.Wordle.CharIsInWord("boses", 'a'));
            Assert.IsFalse(Wordle.Wordle.CharIsInWord("cuzma", 'z'));
            Assert.AreNotEqual(false, Wordle.Wordle.CharIsInWord("huevo", 'o'));
        }


        //CharInCorrectPosition Tests
        [Test]
        public void CharInCorrectPositionPassingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.CharInCorrectPosition('d', 'd'));
            Assert.IsTrue(Wordle.Wordle.CharInCorrectPosition('g', 'g'));
            Assert.IsFalse(Wordle.Wordle.CharInCorrectPosition('u', 'b'));
            Assert.AreNotEqual(true, Wordle.Wordle.CharInCorrectPosition('e', '2'));
        }

        [Test]
        public void CharInCorrectPositionFailingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.CharInCorrectPosition('i', 'u'));
            Assert.IsTrue(Wordle.Wordle.CharInCorrectPosition('s', 'a'));
            Assert.IsFalse(Wordle.Wordle.CharInCorrectPosition('z', 'z'));
            Assert.AreNotEqual(false, Wordle.Wordle.CharInCorrectPosition('o', 'o'));
        }


        //WinCondition Tests
        [Test]
        public void WinConditionPassingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.WinCondition("cadiz", "cadiz"));
            Assert.IsTrue(Wordle.Wordle.WinCondition("mango", "mango"));
            Assert.IsFalse(Wordle.Wordle.WinCondition("atada", "autos"));
            Assert.AreNotEqual(true, Wordle.Wordle.WinCondition("2", "joven"));
        }

        [Test]
        public void WinConditionFailingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.WinCondition("asido", "agita"));
            Assert.IsTrue(Wordle.Wordle.WinCondition("busca", "boses"));
            Assert.IsFalse(Wordle.Wordle.WinCondition("cuzma", "cuzma"));
            Assert.AreNotEqual(false, Wordle.Wordle.WinCondition("huevo", "huevo"));
        }


        //CheckSizeWord Tests
        [Test]
        public void CheckSizeWordPassingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.CheckSizeWord("cadiz", 5));
            Assert.IsTrue(Wordle.Wordle.CheckSizeWord("mango", 5));
            Assert.IsFalse(Wordle.Wordle.CheckSizeWord("atada", 4));
            Assert.AreNotEqual(true, Wordle.Wordle.CheckSizeWord("2", 15));
        }

        [Test]
        public void CheckSizeWordFailingTest()

        {
            Assert.AreEqual(true, Wordle.Wordle.CheckSizeWord("asido", 7));
            Assert.IsTrue(Wordle.Wordle.CheckSizeWord("busca", 3));
            Assert.IsFalse(Wordle.Wordle.CheckSizeWord("cuzma", 5));
            Assert.AreNotEqual(false, Wordle.Wordle.CheckSizeWord("huevo", 5));
        }


        //ConsumirIntento Tests
        [Test]
        public void ConsumirIntentoPassingTest()

        {
            Assert.AreEqual(4, Wordle.Wordle.ConsumirIntento(5));
            Assert.AreNotEqual(7, Wordle.Wordle.ConsumirIntento(12));
        }

        [Test]
        public void ConsumirIntentoFailingTest()

        {
            Assert.AreEqual(9, Wordle.Wordle.ConsumirIntento(7));
            Assert.AreNotEqual(3, Wordle.Wordle.ConsumirIntento(-2));
        }


        //LoadGame Tests
        [Test]
        public void LoadGamePassingTest()

        {
            string[] palabras = { "comer", "lagos", "animo", "bruta", "busca", "pizca", "zombi", "velas", "pieza"};
            Assert.AreEqual("bruta", Wordle.Wordle.LoadGame(palabras, 3));
            Assert.AreNotEqual("lagos", Wordle.Wordle.LoadGame(palabras, 5));
        }

        [Test]
        public void LoadGameFailingTest()

        {
            string[] palabras = { "comer", "lagos", "animo", "bruta", "busca", "pizca", "zombi", "velas", "pieza" };
            Assert.AreEqual("zombi", Wordle.Wordle.LoadGame(palabras, 0));
            Assert.AreNotEqual("pizca", Wordle.Wordle.LoadGame(palabras, 5));
        }


        //GetIntentosTotales Tests
        [Test]
        public void GetIntentosTotalesPassingTest()

        {
            Assert.AreEqual(2, Wordle.Wordle.GetIntentosTotales(5, 3));
            Assert.AreNotEqual(0, Wordle.Wordle.GetIntentosTotales(1, 5));
        }

        [Test]
        public void GetIntentosTotalesFailingTest()

        {
            Assert.AreEqual(2, Wordle.Wordle.GetIntentosTotales(1, 1));
            Assert.AreNotEqual(5, Wordle.Wordle.GetIntentosTotales(7, 2));
        }
    }
}